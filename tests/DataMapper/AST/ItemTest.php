<?php

namespace DataMapper\AST;

use DataMapper\Types;

class ItemTest extends \PHPUnit_Framework_TestCase
{

    private $name;
    private $value;
    private $type;

    protected function setUp()
    {
        $this->name = 'some_name';
        $this->value = 10;
        $this->type = Types::INTEGER;
    }

    public function testCreate()
    {
        $instance = new Item($this->name, $this->value);

        $this->assertInstanceOf(Item::class, $instance);
    }

    public function testCreateStatic()
    {
        $instance = Item::newValue($this->name, $this->value);

        $this->assertInstanceOf(Item::class, $instance);
    }

    public function testGetValue()
    {
        $instance = Item::newValue($this->name, $this->value);

        $this->assertEquals($this->value, $instance->getValue());
    }

    public function testGetType()
    {
        $instance = Item::newValue($this->name, $this->value);

        $this->assertEquals($this->type, $instance->getType());
    }

    public function testGetName()
    {
        $instance = Item::newValue($this->name, $this->value);

        $this->assertEquals($this->name, $instance->getName());
    }

}
