<?php

namespace DataMapper\AST\Parser;

use DataMapper\AST\Tree\TreeInterface;

class ArrayTreeParserTest extends \PHPUnit_Framework_TestCase
{

    private $storage;
    private $data;

    protected function setUp()
    {
        $this->data = [
            'a',
            'b',
            'c',
            'key' => 'value'
        ];

        $this->storage = $this
            ->getMockBuilder(TreeInterface::class)
            ->setMethods([
                'addObject',
                'count',
                'current',
                'rewind',
                'next',
                'key',
                'valid',
            ])
            ->getMock();
    }

    public function testParse()
    {
        $parser = ArrayTreeParser::getParser($this->storage, []);
        $tree = $parser->parse();

        $this->assertSame($this->storage, $tree);
    }

    public function testFill()
    {
        $this->storage
            ->expects($this->exactly(4))
            ->method('addObject')
        ;

        $parser = ArrayTreeParser::getParser($this->storage, $this->data);
        $parser->parse();
    }

    public function testFilledDataCount()
    {
        $storage = new FakeTree();
        $parser = ArrayTreeParser::getParser($storage, $this->data);
        $tree = $parser->parse();

        $this->assertEquals($tree->count(), count($this->data));
    }

    public function testFilledDataKeysEqualsMainData()
    {
        $storage = new FakeTree();
        $parser = ArrayTreeParser::getParser($storage, $this->data);
        $tree = $parser->parse();

        $keys = [];
        foreach ($tree as $item) {
            $keys[] = $item['name'];
        }

        $dataKeys = array_keys($this->data);

        $this->assertEquals(0, count(array_diff_assoc($keys, $dataKeys)));
    }

    public function testFilledData()
    {
        $storage = new FakeTree();
        $parser = ArrayTreeParser::getParser($storage, $this->data);
        $tree = $parser->parse();

        foreach ($tree as $item) {
            $this->assertEquals($this->data[$item['name']], $item['value']);
        }
    }

}
