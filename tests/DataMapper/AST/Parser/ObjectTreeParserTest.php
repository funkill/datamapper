<?php

namespace DataMapper\AST\Parser;

use DataMapper\AST\Tree\TreeInterface;

class ObjectTreeParserTest extends \PHPUnit_Framework_TestCase
{

    private $storage;
    private $data;

    protected function setUp()
    {
        $this->data = new \StdClass();
        $this->data->a = 12;
        $this->data->b = null;
        $this->data->c = 'some string';

        $this->storage = $this
            ->getMockBuilder(TreeInterface::class)
            ->setMethods([
                'addObject',
                'count',
                'current',
                'rewind',
                'next',
                'key',
                'valid',
            ])
            ->getMock();
    }

    public function testParse()
    {
        $parser = ObjectTreeParser::getParser($this->storage, []);
        $tree = $parser->parse();

        $this->assertSame($this->storage, $tree);
    }

    public function testFill()
    {
        $this->storage
            ->expects($this->exactly(3))
            ->method('addObject')
        ;

        $parser = ObjectTreeParser::getParser($this->storage, $this->data);
        $parser->parse();
    }

    public function testFilledDataCount()
    {
        $storage = new FakeTree();
        $parser = ObjectTreeParser::getParser($storage, $this->data);
        $tree = $parser->parse();

        $reflectionObject = new \ReflectionObject($this->data);
        $properties = $reflectionObject->getProperties();

        $this->assertEquals($tree->count(), count($properties));
    }

    public function testFilledDataKeysEqualsMainData()
    {
        $storage = new FakeTree();
        $parser = ObjectTreeParser::getParser($storage, $this->data);
        $tree = $parser->parse();

        $keys = [];
        foreach ($tree as $item) {
            $keys[] = $item['name'];
        }

        $dataKeys = [];
        $reflectionObject = new \ReflectionObject($this->data);
        $properties = $reflectionObject->getProperties();
        foreach ($properties as $property) {
            $dataKeys[] = $property->getName();
        }

        $this->assertEquals(0, count(array_diff_assoc($keys, $dataKeys)));
    }

    public function testFilledData()
    {
        $storage = new FakeTree();
        $parser = ObjectTreeParser::getParser($storage, $this->data);
        $tree = $parser->parse();

        foreach ($tree as $item) {
            $this->assertEquals($this->data->{$item['name']}, $item['value']);
        }
    }

}
