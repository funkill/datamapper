<?php

namespace DataMapper\AST\Parser;

use DataMapper\AST\Tree\TreeInterface;

class FakeTree implements TreeInterface
{

    public $data = [];
    private $index = 0;

    public function current()
    {
        return $this->data[$this->index];
    }

    public function next()
    {
        $this->index++;
    }

    public function key()
    {
        return $this->index;
    }

    public function valid()
    {
        return array_key_exists($this->index, $this->data);
    }

    public function rewind()
    {
        $this->index = 0;
    }

    public function count()
    {
        return count($this->data);
    }

    public function addObject($name, $value)
    {
        $this->data[] = [
            'name' => $name,
            'value' => $value,
        ];
    }

}
