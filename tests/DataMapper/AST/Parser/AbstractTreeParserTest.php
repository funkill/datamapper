<?php

namespace DataMapper\AST\Parser;

use DataMapper\AST\Tree\TreeInterface;

class AbstractTreeParserTest extends \PHPUnit_Framework_TestCase
{

    private $storage;

    protected function setUp()
    {
        $this->storage = $this->getMockBuilder(TreeInterface::class)
            ->getMock();
    }

    /**
     * @dataProvider itemsProvider
     */
    public function testInstanciate($itemForParse, $expected)
    {
        $ast = AbstractTreeParser::getParser($this->storage, $itemForParse);

        $this->assertInstanceOf($expected, $ast);
    }

    public function itemsProvider()
    {
        return [
            [ new \StdClass(), ObjectTreeParser::class ],
            [ [], ArrayTreeParser::class ]
        ];
    }

    /**
     * @expectedException \DataMapper\Exceptions\UnsupportedObjectType
     */
    public function testMustThrowExceptionForUnknownType()
    {
        AbstractTreeParser::getParser($this->storage, 123);
    }

}
