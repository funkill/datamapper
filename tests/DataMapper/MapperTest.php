<?php

namespace DataMapper;

class MapperTest extends \PHPUnit_Framework_TestCase
{

    private $data;
    private $destination;

    protected function setUp()
    {
        $this->data = $this->getMockBuilder(\StdClass::class)
            ->getMock();

        $this->destination = $this->getMockBuilder(\StdClass::class)
            ->getMock();
    }

    /**
     * @dataProvider instantiateDataProvider
     */
    public function testInstantiate($data)
    {
        $mapper = new Mapper($data);

        $this->assertInstanceOf(Mapper::class, $mapper);
    }

    public function instantiateDataProvider()
    {
        return [
            [ [] ],
            [ new \StdClass() ],
        ];
    }

    /**
     * @dataProvider notInstantiableData
     * @expectedException \DataMapper\Exceptions\UnsupportedObjectType
     */
    public function testFailInstantiate($data)
    {
        new Mapper($data);
    }

    public function notInstantiableData()
    {
        return [
            [ 1 ],
            [ 'string' ],
            [ null ],
            [ 1.2 ],
            [ true ],
        ];
    }

    /**
     * @dataProvider instantiateDataProvider
     */
    public function testSetDestination($destination)
    {
        $mapper = new Mapper($this->data);
        $mapper->setDestination($destination);
    }

    /**
     * @dataProvider notInstantiableData
     * @expectedException \DataMapper\Exceptions\UnsupportedObjectType
     */
    public function testWrongDestinations($destination)
    {
        $mapper = new Mapper($this->data);
        $mapper->setDestination($destination);
    }

    public function testDestinationReturns()
    {
        $mapper = new Mapper($this->data);
        $result = $mapper->setDestination($this->destination);

        $this->assertEquals($mapper, $result);
    }

    public function testMappingObjectToArray()
    {
        $source = $this->getTestFilledObject();
        $destination = $this->getTestEmptyArray();
        $expected = $this->getTestFilledArray();

        $mapper = new Mapper($source);
        $result = $mapper->setDestination($destination)->map();

        $this->assertEquals($expected, $result);
    }

    public function testMappingArrayToObject()
    {
        $source = $this->getTestFilledArray();
        $destination = $this->getTestEmptyObject();
        $expected = $this->getTestFilledObject();

        $mapper = new Mapper($source);
        $result = $mapper->setDestination($destination)->map();

        $this->assertEquals($expected, $result);
    }

    public function testMappingObjectToObject()
    {
        $source = $this->getTestFilledObject();
        $destination = $this->getTestEmptyObject();
        $expected = $this->getTestFilledObject();

        $mapper = new Mapper($source);
        $result = $mapper->setDestination($destination)->map();

        $this->assertEquals($expected, $result);
    }

    public function testMappingArrayToArray()
    {
        $source = $this->getTestFilledArray();
        $destination = $this->getTestEmptyArray();
        $expected = $this->getTestFilledArray();

        $mapper = new Mapper($source);
        $result = $mapper->setDestination($destination)->map();

        $this->assertEquals($expected, $result);
    }

    private function getTestFilledObject()
    {
        $object = new \StdClass();
        $object->int = 1;
        $object->float = 1.2;
        $object->string = 'string';
        $object->null = null;
        $object->bool = false;

        return $object;
    }

    private function getTestFilledArray()
    {
        return [
        	'int' => 1,
        	'float' => 1.2,
        	'string' => 'string',
        	'null' => null,
        	'bool' => false,
        ];
    }

    private function getTestEmptyArray()
    {
        return [];
    }

    private function getTestEmptyObject()
    {
        $object = new \StdClass();
        $object->int = null;
        $object->float = null;
        $object->string = null;
        $object->null = null;
        $object->bool = null;

        return $object;
    }

}
