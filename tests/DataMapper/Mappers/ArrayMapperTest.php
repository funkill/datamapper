<?php

namespace DataMapper\Mappers;

use DataMapper\AST\Tree\TreeInterface;

class ArrayMapperTest extends \PHPUnit_Framework_TestCase
{

    private $tree;

    protected function setUp()
    {
        $this->tree = $this->getMockBuilder(TreeInterface::class)
            ->setMethods([
                'addObject',
                'count',
                'current',
                'rewind',
                'next',
                'key',
                'valid',
            ])
            ->getMock();
        $this->tree->method('count')->willReturn(0);
        $this->tree->method('current')->willReturn(null);
    }

    public function testInstantiate()
    {
        $mapper = new ArrayMapper();

        $this->assertInstanceOf(ArrayMapper::class, $mapper);
    }

    public function testSetTree()
    {
        $mapper = new ArrayMapper();
        $result = $mapper->setTree($this->tree);

        $this->assertEquals($mapper, $result);
    }

    /**
     * @expectedException \DataMapper\Exceptions\WrongDestinationType
     */
    public function testWrongDestination()
    {
        $mapper = new ArrayMapper();
        $mapper->setTree($this->tree)->mapTo(123);
    }

    public function testMap()
    {
        $mapper = new ArrayMapper();
        $result = $mapper->setTree($this->tree)->mapTo([]);

        $this->assertTrue(is_array($result));
    }

    public function testMapFilled()
    {
        $tree = $this->getFakeTree();

        $mapper = new ArrayMapper();
        $result = $mapper->setTree($tree)->mapTo([]);

        $this->assertEquals(0, count(array_diff_assoc($this->getFixtures(), $result)));
    }

    private function getFakeTree()
    {
        $tree = new FakeItemTree();
        $data = $this->getFixtures();

        foreach ($data as $key => $item) {
            $tree->addObject($key, $item);
        }

        return $tree;
    }

    private function getFixtures()
    {
        return [
            'int' => 1,
            'string' => '2',
            'bool' => true,
            'null' => null,
            'float' => 1.5,
        ];
    }

}
