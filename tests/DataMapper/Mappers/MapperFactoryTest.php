<?php

namespace DataMapper\Mappers;

class MapperFactoryTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @dataProvider instantiableObjects
     */
    public function testResolveObject($object, $expected)
    {
        $actual = MapperFactory::getMapper($object);

        $this->assertInstanceOf($expected, $actual);
    }

    public function instantiableObjects()
    {
        return [
            [ new \StdClass(), ObjectMapper::class ],
            [ [], ArrayMapper::class ],
        ];
    }

    /**
     * @dataProvider notInstantiableData
     * @expectedException \DataMapper\Exceptions\UnsupportedObjectType
     */
    public function testCannotResolve($object)
    {
        MapperFactory::getMapper($object);
    }

    public function notInstantiableData()
    {
        return [
            [ 1 ],
            [ 'string' ],
            [ null ],
            [ 1.2 ],
            [ true ],
        ];
    }

}
