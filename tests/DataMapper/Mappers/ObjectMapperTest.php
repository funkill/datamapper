<?php

namespace DataMapper\Mappers;

use DataMapper\AST\Tree\TreeInterface;

class ObjectMapperTest extends \PHPUnit_Framework_TestCase
{

    private $tree;

    protected function setUp()
    {
        $this->tree = $this->getMockBuilder(TreeInterface::class)
            ->setMethods([
                'addObject',
                'count',
                'current',
                'rewind',
                'next',
                'key',
                'valid',
            ])
            ->getMock();
        $this->tree->method('count')->willReturn(0);
        $this->tree->method('current')->willReturn(null);
    }

    public function testInstantiate()
    {
        $mapper = new ObjectMapper();

        $this->assertInstanceOf(ObjectMapper::class, $mapper);
    }

    public function testSetTree()
    {
        $mapper = new ObjectMapper();
        $result = $mapper->setTree($this->tree);

        $this->assertEquals($mapper, $result);
    }

    /**
     * @expectedException \DataMapper\Exceptions\WrongDestinationType
     */
    public function testWrongDestination()
    {
        $mapper = new ObjectMapper();
        $result = $mapper->setTree($this->tree)->mapTo([]);
    }

    public function testMap()
    {
        $mapper = new ObjectMapper();
        $result = $mapper->setTree($this->tree)->mapTo(new \StdClass());

        $this->assertTrue(is_object($result));
    }

    public function testMapFilled()
    {
        $tree = $this->getFakeTree();

        $mapper = new ObjectMapper();
        $data = $this->getClassForFixtures();
        $result = $mapper->setTree($tree)->mapTo($data);

        foreach ($this->getFixtures() as $name => $value) {
            $this->assertEquals($value, $result->{$name});
        }
    }

    private function getFakeTree()
    {
        $tree = new FakeItemTree();
        $data = $this->getFixtures();

        foreach ($data as $key => $item) {
            $tree->addObject($key, $item);
        }

        return $tree;
    }

    private function getFixtures()
    {
        return [
            'int' => 1,
            'string' => '2',
            'bool' => true,
            'null' => null,
            'float' => 1.5,
        ];
    }

    private function getClassForFixtures()
    {
        $data = $this->getFixtures();
        array_walk($data, function (&$el) {
            $el = null;
        });

        return json_decode(json_encode($data));
    }

}
