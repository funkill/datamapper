<?php

namespace DataMapper;

class TypesTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @dataProvider typesProvider
     */
    public function testDetectType($var, $expected)
    {
        $type = Types::detectType($var);

        $this->assertEquals($expected, $type);
    }

    public function typesProvider()
    {
        return [
            [ true, Types::BOOL ],
            [ false, Types::BOOL ],

            [ 0, Types::INTEGER ],
            [ 15, Types::INTEGER ],
            [ PHP_INT_MAX, Types::INTEGER ],
            [ PHP_INT_MAX * -1, Types::INTEGER ],

            [ 0.0, Types::FLOAT ],
            [ 1.0, Types::FLOAT ],
            [ 15.5, Types::FLOAT ],
            [ 15e+7, Types::FLOAT ],
            [ -15e+7, Types::FLOAT ],

            [ '', Types::STRING ],
            [ '0', Types::STRING ],
            [ 'Some string', Types::STRING ],

            [ [], Types::ARR ],
            [ [ 1 ], Types::ARR ],
            [ [ 12 => 13], Types::ARR ],
            [ [ 'key' => 'value' ], Types::ARR ],

            [ $this->getMockBuilder(static::class), Types::OBJECT ],

            [ null, Types::NULL ],
        ];
    }

}
