<?php

namespace DataMapper;

final class Types
{

    const BOOL = 0;
    const INTEGER = 1;
    const FLOAT = 2;
    const STRING = 3;
    const ARR = 4;
    const OBJECT = 5;
    const NULL = 6;
    const UNKNOWN = 7;

    final public static function detectType($value)
    {
        switch (true) {
            case is_bool($value):
                $type = self::BOOL;
                break;

            case is_int($value):
                $type = self::INTEGER;
                break;

            case is_float($value):
                $type = self::FLOAT;
                break;

            case is_string($value):
                $type = self::STRING;
                break;

            case is_array($value):
                $type = self::ARR;
                break;

            case is_object($value):
                $type = self::OBJECT;
                break;

            case $value === null:
                $type = self::NULL;
                break;

            default:
                $type = self::UNKNOWN;
                break;
        }

        return $type;
    }

}
