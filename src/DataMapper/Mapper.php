<?php

namespace DataMapper;

use DataMapper\AST\Parser\AbstractTreeParser;
use DataMapper\AST\Tree\DefaultTree;
use DataMapper\AST\Tree\TreeInterface;
use DataMapper\Exceptions\UnsupportedObjectType;
use DataMapper\Mappers\MapperFactory;

class Mapper
{

    private $destination;
    private $data;

    public function __construct($source)
    {
        $this->checkObject($source);

        $this->data = $source;
    }

    public function setDestination($destination)
    {
        $this->checkObject($destination);

        $this->destination = $destination;

        return $this;
    }

    private function checkObject($object)
    {
        $allowedTypes = [ Types::OBJECT, Types::ARR ];
        $type = Types::detectType($object);
        if (!in_array($type, $allowedTypes, true)) {
            throw new UnsupportedObjectType();
        }
    }

    public function map()
    {
        $tree = $this->getTree();
        $this->imposeTreeToDestination($tree);

        return $this->destination;
    }

    private function getTree()
    {
        $parser = AbstractTreeParser::getParser(new DefaultTree(), $this->data);

        return $parser->parse();
    }

    private function imposeTreeToDestination(TreeInterface $tree)
    {
        $mapper = MapperFactory::getMapper($this->destination);

        $this->destination = $mapper->setTree($tree)->mapTo($this->destination);
    }

}
