<?php

namespace DataMapper\AST\Tree;

interface TreeInterface extends \Countable, \Iterator
{

    public function addObject($name, $value);

}
