<?php

namespace DataMapper\AST\Tree;

use DataMapper\AST\Item;

class DefaultTree implements TreeInterface
{

    /**
     * @var \SplObjectStorage
     */
    private $storage;

    public function __construct()
    {
        $this->storage = new \SplObjectStorage();
    }

    public function current()
    {
        return $this->storage->current();
    }

    public function next()
    {
        $this->storage->next();
    }

    public function key()
    {
        return $this->storage->key();
    }

    public function valid()
    {
        return $this->storage->valid();
    }

    public function rewind()
    {
        $this->storage->rewind();
    }

    public function count()
    {
        return $this->storage->count();
    }

    public function addObject($name, $value)
    {
        $item = Item::newValue($name, $value);
        $this->storage->attach($item);
    }

}
