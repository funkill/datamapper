<?php

namespace DataMapper\AST;

use DataMapper\Types;

final class Item
{

    /**
     * @var int
     */
    private $type;

    /**
     * @var string
     */
    private $name;

    /**
     * @var mixed
     */
    private $value;

    public static function newValue($name, $value)
    {
        return new static($name, $value);
    }

    public function __construct($name, $value)
    {
        $this->name = $name;
        $this->value = $value;
        $this->type = Types::detectType($this->value);
    }

    /**
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

}
