<?php

namespace DataMapper\AST\Parser;

class ObjectTreeParser extends AbstractTreeParser
{

    /**
     * @var \ReflectionObject
     */
    private $reflectionObject;

    public function parse()
    {
        $this->prepareData();
        $reflection = $this->reflectionObject;
        $properties = $reflection->getProperties();
        foreach ($properties as $property) {
            if (!$property->isPublic()) {
                $getter = $this->getGetterForProperty($property);
                if (!$getter) {
                    continue;
                }

                $value = $getter->invoke($this->data);
            } else {
                $value = $property->getValue($this->data);
            }

            $this->tree->addObject($property->getName(), $value);
        }

        return $this->tree;
    }

    private function prepareData()
    {
        $this->reflectionObject = new \ReflectionObject($this->data);
    }

    private function getGetterForProperty(\ReflectionProperty $property)
    {
        $name = $property->getName();
        $getter = 'get' . ucfirst($name);
        $reflectionObject = $this->reflectionObject;
        if (!$reflectionObject->hasMethod($getter)) {
            return null;
        }

        $method = $reflectionObject->getMethod($getter);
        if (!$method->isPublic()) {
            return null;
        }

        return $method;
    }

}
