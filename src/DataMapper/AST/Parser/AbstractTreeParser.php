<?php

namespace DataMapper\AST\Parser;

use DataMapper\AST\Tree\TreeInterface;
use DataMapper\Types;
use DataMapper\Exceptions\UnsupportedObjectType;

abstract class AbstractTreeParser implements TreeParserInterface
{
    /**
     * @var mixed
     */
    protected $data;

    /**
     * @var TreeInterface
     */
    protected $tree;

    final private function __construct(TreeInterface $tree, $data)
    {
        $this->tree = $tree;
        $this->data = $data;
    }

    final public static function getParser(TreeInterface $tree, $data)
    {
        $type = Types::detectType($data);
        switch ($type) {
            case Types::OBJECT:
                $parser = new ObjectTreeParser($tree, $data);
                break;
            case Types::ARR:
                $parser = new ArrayTreeParser($tree, $data);
                break;
            default:
                throw new UnsupportedObjectType('Source data must be object or array');
        }

        return $parser;
    }

}
