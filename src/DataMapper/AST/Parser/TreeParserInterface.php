<?php

namespace DataMapper\AST\Parser;

use DataMapper\AST\Tree\TreeInterface;

interface TreeParserInterface
{

    /**
     * @return TreeInterface
     */
    public function parse();

}
