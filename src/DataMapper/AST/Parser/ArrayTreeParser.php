<?php

namespace DataMapper\AST\Parser;

class ArrayTreeParser extends AbstractTreeParser
{

    public function parse()
    {
        foreach ($this->data as $key => $item) {
            $this->tree->addObject($key, $item);
        }

        return $this->tree;
    }

}
