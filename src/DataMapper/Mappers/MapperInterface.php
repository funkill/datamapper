<?php

namespace DataMapper\Mappers;

use DataMapper\AST\Tree\TreeInterface;

interface MapperInterface
{

    /**
     * @param TreeInterface $tree
     *
     * @return static
     */
    public function setTree(TreeInterface $tree);

    public function mapTo($destination);

}
