<?php

namespace DataMapper\Mappers;

use DataMapper\Types;
use DataMapper\Exceptions\UnsupportedObjectType;

class MapperFactory
{

    public static function getMapper($data)
    {
        $type = Types::detectType($data);
        switch ($type) {
            case Types::ARR:
                $mapper = new ArrayMapper();
                break;
            case Types::OBJECT:
                $mapper = new ObjectMapper();
                break;
            default:
                throw new UnsupportedObjectType();
        }

        return $mapper;
    }

}
