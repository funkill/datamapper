<?php

namespace DataMapper\Mappers;

use DataMapper\AST\Item;
use DataMapper\AST\Tree\TreeInterface;
use DataMapper\Exceptions\WrongDestinationType;

class ArrayMapper implements MapperInterface
{

    private $tree;

    public function setTree(TreeInterface $tree)
    {
        $this->tree = $tree;

        return $this;
    }

    public function mapTo($destination)
    {
        if (!is_array($destination)) {
            throw new WrongDestinationType('Destination must be array!');
        }

        $result = $destination;
        /**
         * @var Item $leaf
         */
        foreach ($this->tree as $leaf) {
            $result[$leaf->getName()] = $leaf->getValue();
        }

        return $result;
    }

}
