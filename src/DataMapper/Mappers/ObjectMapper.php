<?php

namespace DataMapper\Mappers;

use DataMapper\AST\Item;
use DataMapper\AST\Tree\TreeInterface;
use DataMapper\Exceptions\WrongDestinationType;

class ObjectMapper implements MapperInterface
{

    private $tree;

    /**
     * @var \ReflectionObject
     */
    private $reflectedDestination;

    public function mapTo($destination)
    {
        $this->checkDestination($destination);
        $this->prepareDestination($destination);

        /**
         * @var Item $item
         */
        foreach ($this->tree as $item) {
            $property = $this->getPropertyForName($item->getName());
            if (!$property) {
                continue;
            }

            if ($property->isPublic()) {
                $destination->{$property->getName()} = $item->getValue();

                continue;
            }

            $setter = $this->getSetterForPropertyName($property->getName());
            if (!$setter) {
                continue;
            }

            $destination->{$setter}($item->getValue());
        }

        return $destination;
    }

    public function setTree(TreeInterface $tree)
    {
        $this->tree = $tree;

        return $this;
    }

    /**
     * @param $destination
     *
     * @throws \Exception
     */
    private function checkDestination($destination)
    {
        if (!is_object($destination)) {
            throw new WrongDestinationType('Destination must be object!');
        }
    }

    private function prepareDestination($destination)
    {
        $this->reflectedDestination = new \ReflectionObject($destination);
    }

    private function getPropertyForName($name)
    {
        if (!$this->reflectedDestination->hasProperty($name)) {
            return null;
        }

        return $this->reflectedDestination->getProperty($name);
    }

    private function getSetterForPropertyName($name)
    {
        $name = 'set' . ucfirst($name);
        if (!$this->reflectedDestination->getMethod($name)) {
            return null;
        }

        return $name;

    }

}
